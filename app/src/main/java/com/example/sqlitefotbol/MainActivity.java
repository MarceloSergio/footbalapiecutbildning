package com.example.sqlitefotbol;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //create a new instance, takes one argument context this, constructor will create data base and table
    //create an instance of the class with instance name myDb
    DatabaseHelper myDb;
    //Define 3 variables for Edit Text and one variable for Button
    EditText editName, editEmail;
    RadioGroup radioSexGroup;
    RadioButton radioSexButton;
    Button btnAddData;
    ScrollView footballClub;

    //calling method addData

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myDb = new DatabaseHelper(this);

        //cast data into variables
        editName = (EditText)findViewById(R.id.editText1_name);
        editEmail = (EditText)findViewById(R.id.editText2_email);
        footballClub = (ScrollView) findViewById(R.id.scrollView1_FootballClub);
        radioSexGroup =(RadioGroup) findViewById(R.id.radioSex);
        //button to call some action
        btnAddData = (Button) findViewById(R.id.button_add);
        addData();
    }

    public void addData(){
        btnAddData.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //isInserted return true or false
                        boolean isInserted = myDb.insertData(editName.getText().toString().trim(),
                                editEmail.getText().toString().trim(),
                                footballClub.toString().trim(), addListenerOnButtonSex(radioSexGroup));
                        if(isInserted == true){
                            Toast.makeText(MainActivity.this,"Data Inserted", Toast.LENGTH_LONG).show();
                        }else {
                            Toast.makeText(MainActivity.this,"Data not Inserted", Toast.LENGTH_LONG).show();
                        }
                    }
                }
        );
    }

    public String addListenerOnButtonSex(RadioGroup radioSexGroup){
        //get selected radio button from radio group
        int selectedId = radioSexGroup.getCheckedRadioButtonId();
        //find the radiobutton by returned id
        radioSexButton = (RadioButton) findViewById(selectedId);
        //returns Male or Female in the Column Gender
        return radioSexButton.getText().toString();
    }

}
