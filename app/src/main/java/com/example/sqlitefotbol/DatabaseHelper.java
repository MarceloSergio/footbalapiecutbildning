package com.example.sqlitefotbol;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

//to create and upgrade a database in android application I create a subclass of the SQLiteOpenHelper.java class
public class DatabaseHelper extends SQLiteOpenHelper {

    //to create SQLite data base we need data base name player, id, name, surname, credits
    //declare variables to assign the database name and column names, table name
    public static final String DATABASE_NAME = "Player.db";
    public static final String TABLE_NAME = "player_table";
    public static final String COLUMN_1 = "ID";
    public static final String COLUMN_2 = "NAME";
    public static final String COLUMN_3 = "EMAIL";
    public static final String COLUMN_4 = "FAVORITE_FOOTBALL_CLUB";
    public static final String COLUMN_5 = "SEX";

    //DEFAULT CONSTRUCTOR
    //create database in the constructor, when constructor is called the data base will be created, create function
    //public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version), for simplicity just context
    /*@param context to use for locating paths to the the database
     * @param name of the database file, or null for an in-memory database
     * @param factory to use for creating cursor objects, or null for the default
     * @param version number of the database (starting at 1); if the database is older,
     *     {@link #onUpgrade} will be used to upgrade the database; if the database is
     *     newer, {@link #onDowngrade} will be used to downgrade the database*/
    /*Create a helper object to create, open, and/or manage a database.
            * The database is not actually created or opened until one of
     * {@link #getWritableDatabase} or {@link #getReadableDatabase} is called.*/

    //constructor to create data base and table
    public DatabaseHelper(Context context){
        //super class
        super (context, DATABASE_NAME, null, 1);
        //create an instance of SQLiteDataBase
        //create database and table
        //SQLiteDatabase db = this.getWritableDatabase();//later this will be removed, is just for checking
    }

    //Implement methods. override abstract methods in class SQLiteOpenHelper.java, abstract methods has no definition
    @Override
    public void onCreate (SQLiteDatabase db){
        //it takes the string variable or string query
        //db.execSQL("create table " + TABLE_NAME +" ("+ COLUMN_1+")" )
        //create a table using this query, table with 4 columns, id increments automatically if you do not give data to it, type: integer, text
        db.execSQL("create table " + TABLE_NAME +" (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, EMAIL TEXT, FAVORITE_FOOTBALL_CLUB TEXT, SEX TEXT)" );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    //method to insert data into the table
    //method insert: @return the row ID of the newly inserted row, or -1 if an error occurred
    public boolean insertData(String name, String email, String favorite_football_club, String sex){
        //create instance of SQLite database in our insert data method
        SQLiteDatabase db = this.getWritableDatabase();
        //content values to put some data into the columns
        ContentValues contentValues = new ContentValues();
        //put method accepts two parameters: string key , byte value that we will pass
        contentValues.put(COLUMN_2, name);
        contentValues.put(COLUMN_3, email);
        contentValues.put(COLUMN_4, favorite_football_club);
        contentValues.put(COLUMN_5, sex);
        //insert the values using db instance, contentValues in the table TABLE_NAME, insert method with 4 arguments in class SQLiteDatabase.java
        //in case of error returns -1
        //Go To Declaration to find the method , rigth click
        //if the data is not inserted the method insert will return -1
        long result = db.insert(TABLE_NAME, null, contentValues);
        if (result == -1){
            return false;
        }else
            return true;
    }
}
